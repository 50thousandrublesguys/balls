﻿package model
{
	import flash.display.Sprite;
	
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2Fixture;

	public class Ring
	{
		public static var radius:Number;
		
		private var _body:b2Body;
		private var _bodyDef:b2BodyDef;
		public var view:Sprite;
		
		private var _gates:Vector.<b2Fixture>;
		private var _walls:Vector.<b2Fixture>;
		
		public function Ring()
		{
			_bodyDef = new b2BodyDef();
			_bodyDef.type = b2Body.b2_kinematicBody;
			_bodyDef.allowSleep = false;
			
			_gates = new Vector.<b2Fixture>();
			_walls = new Vector.<b2Fixture>();
		}
		
		public function get gates():Vector.<b2Fixture>
		{
			return _gates;
		}

		public function get bodyDef():b2BodyDef
		{
			return _bodyDef;
		}

		public function get body():b2Body
		{
			return _body;
		}

		public function set body(value:b2Body):void
		{
			_body = value;

			createFixtures();
		}
		
		private function createFixtures() : void
		{
			var r:Number = Ring.radius;
			var rSmall:Number = r-0.1; //меньший радиус, 0.2 - толщина стенки
			
			var horda:Number = (Ball.radius + 0.4) * 2; //хорда, дырка под шарик с запасом
			//центральный угол дырки
			var ballHoleAngle:Number = 2 * Math.asin(horda/(2*r));
			//центральный угол стены
			var wallHoleAngle:Number = (2 * Math.PI - 3 * ballHoleAngle) / 3;
			
			//суть построения такова: наращиваем угол, попутно вычисляем точки из которых строим полигон
			var angles:Array = []; //соберём опорные углы в кучу
			var total:Number = 0;
			for(var i:int = 0; i<6; i++)
			{
				if(i%2 == 0)
				{
					total += wallHoleAngle;
				}
				else
				{
					total += ballHoleAngle;
				}
				angles.push(total);
			}
			//в сумме они должны составлять 2Pi =)
			
			//начинаем проходку
			var angle:Number = 0;//центральный угол
			var index:int = 0;
			var currentAngle:Number = 0;
			var pX:Number;
			var pY:Number;
			var pXsmall:Number;
			var pYsmall:Number;
			var points:Vector.<b2Vec2> = new Vector.<b2Vec2>();//точки по большему радиусу
			var pointsSmall:Vector.<b2Vec2> = new Vector.<b2Vec2>();//точки по меньшему радиусу
			var maxAngle = 2*Math.PI;
			while(angle <= maxAngle)
			{
				if(angle == 0)
				{
					pX = r * Math.cos(angle);
					pY = r * Math.sin(angle);
					pXsmall = rSmall * Math.cos(angle);
					pYsmall = rSmall * Math.sin(angle);
					//отмечаем первую точку
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
				}
				
				//итерируем
				angle += 0.01;
				currentAngle += 0.01;
				//смотрим-проверяем
				if(angle >= angles[index])
				{
					angle = angles[index];
					
					//дошли до конца сегмента
					//пришло время определить точку и выставить её
					pX = r * Math.cos(angle);
					pY = r * Math.sin(angle);
					pXsmall = rSmall * Math.cos(angle);
					pYsmall = rSmall * Math.sin(angle);
					//отмечаем точку
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
					
					//смена сегмента дуги, теперь нам надо построить фикстуру по точкам
					//строится фикстура по часовой стрелке, т.о. мы объединяем массивы pointsSmall и points, 
					//при этом pointsSmall делаем reverse()
					pointsSmall = pointsSmall.reverse();
					points = points.concat(pointsSmall);
					if(index % 2 == 0)
					{
						createSegmentFixture(points);
					}
					else
					{
						createGateFixture(points);
					}
					
					currentAngle = 0;
					points = new Vector.<b2Vec2>();
					pointsSmall = new Vector.<b2Vec2>();
					//при этом в той конечной точке берёт начало уже другая фикстура
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
					index++;
				}
				else if(currentAngle >= ballHoleAngle)
				{
					var calculatedAngle:Number = angle - (currentAngle - ballHoleAngle);
					//пришло время определить точку и выставить её
					pX = r * Math.cos(calculatedAngle);
					pY = r * Math.sin(calculatedAngle);
					pXsmall = rSmall * Math.cos(calculatedAngle);
					pYsmall = rSmall * Math.sin(calculatedAngle);
					//отмечаем точку
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
					
					pointsSmall = pointsSmall.reverse();
					points = points.concat(pointsSmall);
					
					createSegmentFixture(points);
					
					points = new Vector.<b2Vec2>();
					pointsSmall = new Vector.<b2Vec2>();
					//при этом в той конечной точке берёт начало уже другая фикстура
					points.push(new b2Vec2(pX, pY));
					pointsSmall.push(new b2Vec2(pXsmall, pYsmall));
					
					currentAngle = 0;
				}
			}
		}
		
		private function createGateFixture(points:Vector.<b2Vec2>):void
		{
			trace("createGateFixture");
			//создаём фикстуру дырки, через которую будет пролезать шарик
			//фикстура для детекции пролез шарик или нет, если пролез, то столкновений с ней не будет
			//если застрял в ней, то столкновения есть и шарик надо вернуть на исходную позицию
			var s:b2PolygonShape = b2PolygonShape.AsVector(points, points.length);
			var f:b2Fixture = _body.CreateFixture2(s, 5);
			var filter:b2FilterData = new b2FilterData();
			filter.categoryBits = CategoryEnum.GATE;
			filter.maskBits = CategoryEnum.BALL;
			f.SetFilterData(filter);
			_gates.push(f);
		}
		
		private function createSegmentFixture(points:Vector.<b2Vec2>):void
		{
			trace("createSegmentFixture");
			//создаём полигональную форму
			var s:b2PolygonShape = b2PolygonShape.AsVector(points, points.length);
			//добавляем эту фикстуру к телу
			var f:b2Fixture = _body.CreateFixture2(s, 500000);
			var filter:b2FilterData = new b2FilterData();
			filter.categoryBits = CategoryEnum.WALL;
			filter.maskBits = CategoryEnum.BALL + CategoryEnum.FIXED_BALL;
			f.SetFilterData(filter);
			_walls.push(f);
		}
		
		public function update() : void
		{
			_body.SetAngularVelocity(Config.RING_SPEED);
			
			view.x = _body.GetPosition().x * Config.BOXSCALE;
			view.y = _body.GetPosition().y * Config.BOXSCALE;
			view.rotation = _body.GetAngle()*180/Math.PI;
		}
		
		public function checkAndReturnFixture(fixture:b2Fixture) : b2Fixture
		{
			var gate:b2Fixture;
			for each(var gf:b2Fixture in _gates)
			{
				if(gf == fixture)
				{
					gate = gf;
					break;
				}
			}
			return gate;
		}
	}
}