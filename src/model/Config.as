package model
{
	public class Config
	{
		public static var BOXSCALE:Number = 30;
		public static var MAX_TIMER:int = 30000;
		//дистанция зачета шарика
		public static var MAX_DISTANCE:Number = 2.2;
		public static var RING_SPEED:Number = 0.5;
		public static var MOUSE_SPEED:Number = 300000;
		
		public static var BALL_DAMPING:Number = 0.5;
	}
}