﻿package model
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.utils.getDefinitionByName;
	
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2FilterData;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	
	import caurina.transitions.Tweener;
	
	import fl.transitions.Tween;

	public class Ball
	{
		public static var radius:Number;
		private static var _bodyDef:b2BodyDef;

		private var _startPoint:b2Vec2;
		private var _startPosition:int = -1;
		private var _color:Number;
		private var _body:b2Body;
		private var _fixture:b2Fixture;
		private var _view:Sprite;
		
		private var _fixedBall:Boolean = true;
		
		public function get fixedBall():Boolean
		{
			return _fixedBall;
		}

		public function get fixture():b2Fixture
		{
			return _fixture;
		}

		public function get startPoint():b2Vec2
		{
			return _startPoint;
		}

		/**
		 * Стартовая позиция, одна из 4-х в зависимости от позиции шарик можно таскать только в определенном направлении 
		 * @return 
		 * 
		 */		
		public function get startPosition():int
		{
			return _startPosition;
		}
		public function set startPosition(value:int):void
		{
			_startPosition = value;
			_startPoint = _body.GetPosition().Copy();
			
			var filter:b2FilterData;
			if(_startPosition >= 0)
			{
				markAsFixed();
			}
			else
			{
				_fixedBall = false;
				filter = new b2FilterData();
				filter.categoryBits = CategoryEnum.BALL;
				filter.maskBits = CategoryEnum.WALL + CategoryEnum.GATE + CategoryEnum.BALL + CategoryEnum.FIXED_BALL;
				_fixture.SetFilterData(filter);	
			}
			_body.SetUserData(this);
		}

		public function get view():Sprite
		{
			return _view;
		}
		
		public static function get bodyDef():b2BodyDef
		{
			_bodyDef = new b2BodyDef();
			_bodyDef.type = b2Body.b2_dynamicBody;
			_bodyDef.fixedRotation = true;
			_bodyDef.linearDamping = Config.BALL_DAMPING;
			_bodyDef.angularDamping = Config.BALL_DAMPING;
			_bodyDef.allowSleep = false;
			return _bodyDef;
		}
		
		public function get body():b2Body
		{
			return _body;
		}
		
		public function set body(value:b2Body):void
		{
			_body = value;
			var shape:b2CircleShape = new b2CircleShape(Ball.radius);
			_fixture = _body.CreateFixture2(shape, 5);
			markAsFixed();
			_body.SetUserData(this);
			
		}
		
		public function Ball(ballColor:Number = -1)
		{
			//pick random color
			if(ballColor == -1)
			{
				var i:int = Math.random() * ColorEnum.colors.length;
				ballColor = ColorEnum.colors[i];
			}
			_color = ballColor;
			var viewClass:Class = getDefinitionByName("ballType"+ballColor) as Class;
			_view = new viewClass();
			_view.visible = false;
		}
		
		public function markAsFixed(value:Boolean = true) : void
		{
			_fixedBall = value;
			var filter:b2FilterData = new b2FilterData();
			if(value)
			{
				filter.categoryBits = CategoryEnum.FIXED_BALL;
				filter.maskBits = CategoryEnum.WALL + CategoryEnum.BALL + CategoryEnum.FIXED_BALL;
			}
			else
			{
				filter.categoryBits = CategoryEnum.BALL;
				filter.maskBits = CategoryEnum.WALL + CategoryEnum.GATE + CategoryEnum.BALL + CategoryEnum.FIXED_BALL;
			}
			_fixture.SetFilterData(filter);
			_body.SetUserData(this);
		}
		
		public function get color():Number
		{
			return _color;
		}
		
		public function update() : void
		{
			if(_view)
			{
				_view.visible = true;
				_view.x = _body.GetPosition().x * Config.BOXSCALE;
				_view.y = _body.GetPosition().y * Config.BOXSCALE;
				_view.rotation = _body.GetAngle()*180/Math.PI;
			}
		}
		
		public function clear(posToDespose:b2Vec2 = null):void
		{
			_body.SetUserData(null);
			_fixture.SetUserData(null);
			if(posToDespose)
			{
				Tweener.addTween(_view, {x:posToDespose.x * Config.BOXSCALE, y:posToDespose.y* Config.BOXSCALE, time:0.4, onComplete: viewHide});
			}
			else
			{
				clearView();
			}
			_body.DestroyFixture(_fixture);
			_fixture = null;
			_body = null;
			_startPoint = null;
		}
		
		private function viewHide() : void
		{
			Tweener.addTween(_view, {scaleX:0.01, scaleY:0.01, time:0.3, onComplete: clearView});
		}
		
		private function clearView() : void
		{
			if(_view && _view.parent)
			{
				_view.parent.removeChild(_view);
				_view.graphics.clear();
			}
			_view = null;
		}
	}
}