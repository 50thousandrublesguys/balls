package model
{
	public class StartPositionEnum
	{
		public static const FREE:int = -1;
		public static const TOP:int = 0;
		public static const RIGHT:int = 1;
		public static const BOTTOM:int = 2;
		public static const LEFT:int = 3;
	}
}