package model
{
	public class CategoryEnum
	{
		public static const NEUTRAL:int = 0;
		public static const USUAL:int = 1;
		public static const BALL:int = 2;
		public static const WALL:int = 4;
		public static const GATE:int = 8;
		public static const FIXED_BALL:int = 16;
	}
}