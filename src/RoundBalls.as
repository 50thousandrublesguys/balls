﻿package
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import Box2D.Collision.b2AABB;
	import Box2D.Collision.b2Point;
	import Box2D.Collision.Shapes.b2CircleShape;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Collision.Shapes.b2Shape;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2World;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.Joints.b2LineJoint;
	import Box2D.Dynamics.Joints.b2LineJointDef;
	import Box2D.Dynamics.Joints.b2MouseJoint;
	import Box2D.Dynamics.Joints.b2MouseJointDef;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	
	import logic.ContactListener;
	
	import model.Ball;
	import model.Config;
	import model.Ring;
	import model.StartPositionEnum;
	
	import view.Console;
	
	public class RoundBalls extends Sprite
	{
		private var _timerTF:TextField;
		private var _scoreTF:TextField;
		private var _scores:int = 0;
		
		private var _balls:Vector.<Ball>;
		private var _ballJoints:Vector.<b2LineJoint>;
		private var _freeBalls:Vector.<Ball>;
		private var _ring:Ring;
		
		private var _ballsToDelete:Array = [];
		
		private var _world:b2World;
		private var _debugSprite:Sprite;
		private var _debugDraw:b2DebugDraw;
		
		private var _ballsContainer:Sprite;
		private var _ringJoint:b2RevoluteJoint;
		private var _axisBody:b2Body;
		
		private var _isMouseDown:Boolean = false; //флаг определяющий что кнопка мыши нажата
		private var _mouseXWorldPhys:Number; //координата мышки по оси X в метрах
		private var _mouseYWorldPhys:Number; //координата мышки по оси Н в метрах
		private var _mouseXWorld:Number; //координата мышки по оси X в пикселях
		private var _mouseYWorld:Number; //координата мышки по оси Y в пикселях
		private var _mousePVec:b2Vec2 = new b2Vec2();
		private var _mouseJoint:b2MouseJoint;
		
		private var _lastTime:int = -1;
		private var _timer:int = 0;
		
		private var _contactListener:ContactListener;
		
		private var _consoleButton:Sprite;
		private var _console:Console;
		
		/**
		 * Если сделаешь фишку с остановкой шара, думаю можно флаг сделать мол включить выключить фишку
		 * И для срабатывания склеивания одинаковых шаров - область думаю тоже контролом нужно вытащить, 
		 * если их там две то обе - т.е. якак понял там расстояние и определенны интервал углового значения
		 * 
		 * добавить анимацию схлопывания шаров
		 * 
		 * ball_fast_stop
		 * timer
		 * sensor_distance
		 * ring_speed
		 * ball_to_finger_force
		 */
		public function RoundBalls()
		{
			if(stage)
			{
				init(null);				
			}
			else
			{
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		private function resetGame() : void
		{
			var ball:Ball;
			removeEventListener(Event.ENTER_FRAME, update);
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, flagCheck);
			stage.removeEventListener(MouseEvent.MOUSE_UP, flagCheck);
			for each(ball in _contactedBalls)
			{
				_contactedBalls[ball] = null;
				delete _contactedBalls[ball];
			}
			_contactedBalls = new Dictionary(true);
			_scores = 0;
			_scoreTF.text = "Score: "+_scores;
			_lastTime = -1;
			_timer = 0;
			_isMouseDown = false;
			if(_mouseJoint)
			{
				_world.DestroyJoint(_mouseJoint);
			}
			
			for each(ball in _balls)
			{
				removeBall(ball);
			}
			for each(ball in _freeBalls)
			{
				removeBall(ball);
			}
			_freeBalls = new Vector.<Ball>();
			_balls = new Vector.<Ball>();
			trace("startGame");
			startGame();
		}
		
		protected function init(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_timerTF = this["timerTF"] as TextField;
			_scoreTF = this["scoreTF"] as TextField;
			
			_console = new Console(this["console"]);
			_console.visible = false;
			_consoleButton = this["consoleButton"] as Sprite;
			_consoleButton.addEventListener(MouseEvent.CLICK, handleConsoleClick);
			
			
			//задаём радиус шарику (места старта соразмерны шарикам)
			Ball.radius = convertToBox(this.start0.width * 0.5);
			//задаём радиус кольцу
			Ring.radius = convertToBox(this.ringBg.width * 0.5);
			
			createWorld();
//			createDebug();
			createRing();
			
			startGame();
		}
		
		private function handleConsoleClick(e:MouseEvent):void 
		{
			_console.visible = !_console.visible;
		}
		
		private function startGame() : void
		{
			createBalls();
			
			addEventListener(Event.ENTER_FRAME, update, false, 0, true);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, flagCheck);
			stage.addEventListener(MouseEvent.MOUSE_UP, flagCheck);
			
			_contactListener = new ContactListener();
			_contactListener.beginCallback = handleBeginContact;
			_contactListener.endCallback = handleEndContact;
			_world.SetContactListener(_contactListener);
		}
		
		private var _contactedBalls:Dictionary = new Dictionary(true);
		private function handleEndContact(contact:b2Contact):void 
		{
			var gateFixture:b2Fixture;
			var ball:Ball;
			var isGateA:Boolean = true;
			gateFixture = _ring.checkAndReturnFixture(contact.GetFixtureA());
			if(!gateFixture)
			{
				gateFixture = _ring.checkAndReturnFixture(contact.GetFixtureB());
				if(gateFixture)
				{
					isGateA = false;
				}
			}
			if(!gateFixture)
			{
				return;
			}
			
			//у нас есть фикстура ворот, проверим, если вторая фикстура мяч - можно двигаться дальше
			if(isGateA && contact.GetFixtureB().GetBody().GetUserData() is Ball)
			{
				ball = contact.GetFixtureB().GetBody().GetUserData() as Ball;
				if(ball.startPosition >= 0)
				{
					ball = null;
				}
			}
			if(!isGateA && contact.GetFixtureA().GetBody().GetUserData() is Ball)
			{
				ball = contact.GetFixtureA().GetBody().GetUserData() as Ball;
				if(ball.startPosition >= 0)
				{
					ball = null;
				}
			}
			if(!ball)
			{
				return;
			}
			trace("endContact!", ball.color);
			//удаляем инфу о контакте
			ball.markAsFixed(false);
			_contactedBalls[ball] = null;
			delete _contactedBalls[ball];
		}
		private function handleBeginContact(contact:b2Contact) : void
		{
			var gateFixture:b2Fixture;
			var ball:Ball;
			var isGateA:Boolean = true;
			gateFixture = _ring.checkAndReturnFixture(contact.GetFixtureA());
			if(!gateFixture)
			{
				gateFixture = _ring.checkAndReturnFixture(contact.GetFixtureB());
				if(gateFixture)
				{
					isGateA = false;
				}
			}
			if(gateFixture)
			{
				trace("GateFixtureAtA", isGateA);
				//у нас есть фикстура ворот, проверим, если вторая фикстура мяч - можно двигаться дальше
				if(isGateA && contact.GetFixtureB().GetBody().GetUserData() is Ball)
				{
					ball = contact.GetFixtureB().GetBody().GetUserData() as Ball;
					if(ball.startPosition >= 0)
					{
						ball = null;
					}
				}
				if(!isGateA && contact.GetFixtureA().GetBody().GetUserData() is Ball)
				{
					ball = contact.GetFixtureA().GetBody().GetUserData() as Ball;
					if(ball.startPosition >= 0)
					{
						ball = null;
					}
				}
				if(!ball)
				{
					return;
				}
				
				trace("BallAtA", !isGateA);
				//добавляем инфу о контакте
				_contactedBalls[ball] = ball;
			}
		}
		
		private function calctulateCollisions(ball:Ball) : void
		{
			var ballPos:Point = new Point(ball.body.GetPosition().x, ball.body.GetPosition().y);
			var curPos:Point;
			var cur:Number;
			var min:Number = Number.MAX_VALUE;
			var minBall:Ball;
			for each(var startBall:Ball in _balls)
			{
				if(startBall.color == ball.color)
				{
					curPos = new Point(startBall.body.GetPosition().x, startBall.body.GetPosition().y);
					cur = Point.distance(ballPos, curPos);
					if(cur < min)
					{
						min = cur;
						minBall = startBall;
					}
				}
			}
			
			if(!minBall)
			{
				trace("noBall");
				return;
			}
			
			trace("MinDistance:", min, Config.MAX_DISTANCE, minBall.color, ball.color);
			
			if(min < Config.MAX_DISTANCE)
			{
				trace("addScore!");
				_scores++;
				_scoreTF.text = "Score: "+_scores;
//				ball.markAsFixed();
				if(_mouseJoint && _mouseJoint.GetBodyB() == ball.body)
				{
					_world.DestroyJoint(_mouseJoint);
				}
				_contactedBalls[ball] = null;
				delete _contactedBalls[ball];
				removeBall(ball, minBall.body.GetPosition().Copy());
			}
			//если есть коллизия между Gate и Ball
			//ищем ближайший StartPosition к этому Ball
			//если они реально лежат очень близко, значит шарик придвинули, ну либо он сам подошел
			//проверяем цвета
			//если цвета совпали, то удаляем шарик и засчитываем очко
		}
		
		private function createWorld() : void
		{
			_world = new b2World(new b2Vec2(0,0), true);
		}
		
		private function createDebug():void
		{
			_debugSprite = new Sprite();
			addChild(_debugSprite);
			//создаём экземпляр
			_debugDraw = new b2DebugDraw();
			//передаём спрайт для отрисовки
			_debugDraw.SetSprite(_debugSprite);
			//передаём масштаб мира
			_debugDraw.SetDrawScale(Config.BOXSCALE);
			//настраиваем прозрачность
			_debugDraw.SetFillAlpha(0.5);
			//настраиваем толщину линий
			_debugDraw.SetLineThickness(1);
			//выставляем флаги  показа информации
			_debugDraw.SetFlags(b2DebugDraw.e_shapeBit|b2DebugDraw.e_jointBit|b2DebugDraw.e_centerOfMassBit|b2DebugDraw.e_aabbBit|b2DebugDraw.e_controllerBit|b2DebugDraw.e_pairBit);
			//передаём отрисовщик миру
			_world.SetDebugDraw(_debugDraw);
		}
		
		private function createBalls() : void
		{
			_ballsContainer = new Sprite();
			_balls = new Vector.<Ball>();
			_ballJoints = new Vector.<b2LineJoint>();
			addChild(_ballsContainer);
			
			//выставим шары на исходные позиции
			for(var i:int = 0; i<4; i++)
			{
				recteateBallAtStart(i);
			}
		}
		
		private function recteateBallAtStart(index:int) : void
		{
			var ball:Ball;
			var joint:b2LineJoint;
			var jointDef:b2LineJointDef;
			
			ball = createBall();
			_balls[index] = ball;
			ball.body.SetPosition(new b2Vec2(convertToBox(this["start"+index].x), convertToBox(this["start"+index].y)));
			ball.startPosition = index;
			_ballsContainer.addChild(ball.view);
			
			jointDef = new b2LineJointDef(); //создаем определение линейного соединения
			var dir:b2Vec2 = new b2Vec2(0, 1);
			if(index == 1)
			{
				dir	= new b2Vec2(-1, 0);
			}
			else if(index == 2)
			{
				dir = new b2Vec2(0, -1);
			}
			else if(index == 3)
			{
				dir = new b2Vec2(1, 0);
			}
			jointDef.Initialize(_world.GetGroundBody(), ball.body, ball.body.GetPosition(), dir); //инициализируем соединение
			jointDef.motorSpeed = 100;
			jointDef.enableLimit = true; //включаем пределы
			jointDef.lowerTranslation = 0; //нижний предел
			jointDef.upperTranslation = 3; //верхний предел
			_ballJoints[index] = _world.CreateJoint(jointDef) as b2LineJoint; //добавляем соединение в мир	
			
		}
		
		private function createBall() : Ball
		{
			var b:Ball = new Ball();
			b.body = _world.CreateBody(Ball.bodyDef);
			return b;
		}
		private function removeBall(ball:Ball, positionToDestroy:b2Vec2 = null) : void
		{
			if (ball)
			{
				_ballsToDelete.push({ball:ball, pos:positionToDestroy});
			}
		}
		
		private function createRing() : void
		{
			_ring = new Ring();
			_ring.body = _world.CreateBody(_ring.bodyDef);
			_ring.view = this.ringBg;
			_ring.body.SetPosition(new b2Vec2(convertToBox(this.ringBg.x), convertToBox(this.ringBg.y)));
		}
		
		private function createMotor() : void
		{
			var d:b2BodyDef = new b2BodyDef();
			d.position.SetV(_ring.body.GetPosition());
			d.type = b2Body.b2_staticBody;
			_axisBody = _world.CreateBody(d);
			
			var j:b2RevoluteJointDef = new b2RevoluteJointDef();
			j.collideConnected = false;
			j.Initialize(_axisBody, _ring.body, _axisBody.GetWorldCenter()); //инициализируем соединение
			_ringJoint = _world.CreateJoint(j) as b2RevoluteJoint; //добавляем в мир
			_ringJoint.SetMaxMotorTorque(10);//максимальный крутящий момент мотора
			_ringJoint.SetMotorSpeed(0.5); //желаемая скорость вращения в радианах в секунду
			_ringJoint.EnableMotor(true); //включаем мотор
		}
		
		
		protected function update(event:Event):void
		{
			mouseDrag();
			
			//считаем коллизии
			var ball:Ball;
			for each(ball in _contactedBalls)
			{
				calctulateCollisions(ball);
			}
			//удаляем мусор
			var i:int;
			var deletedBall:Object = _ballsToDelete.shift();
			while (deletedBall)
			{
				if(_freeBalls)
				{
					for(i = 0;i<_freeBalls.length;i++)
					{
						if(_freeBalls[i] == deletedBall.ball)
						{
							_freeBalls.splice(i, 1);
							break;
						}
					}
				}
				_world.DestroyBody(deletedBall.ball.body);
				deletedBall.ball.clear(deletedBall.pos);
				
				deletedBall = _ballsToDelete.shift();
			}
			//update
			_ring.update();
			
			var joint:b2LineJoint;
			var ball:Ball;
			for(i = 0; i<_balls.length; i++)
			{
				ball = _balls[i] as Ball;
				var pos:b2Vec2 = ball.body.GetPosition();
				switch(ball.startPosition)
				{
					case StartPositionEnum.TOP:
						if (pos.y >= ball.startPoint.y + 2)
						{
							joint = _ballJoints[i];
						}
						break;
					case StartPositionEnum.RIGHT:
						if (pos.x <= ball.startPoint.x - 2)
						{
							joint = _ballJoints[i];
						}
						break;
					case StartPositionEnum.BOTTOM:
						if (pos.y <= ball.startPoint.y - 2)
						{
							joint = _ballJoints[i];
						}
						break;
					case StartPositionEnum.LEFT:
						if (pos.x >= ball.startPoint.x + 2)
						{
							joint = _ballJoints[i];
						}
						break;
				}
				//нашли джойнт, значит надо его удалить, чтобы шарик летал самостоятельно
				if(joint)
				{
					_world.DestroyJoint(joint);
					joint = null;
					
					if(!_freeBalls)
					{
						_freeBalls = new Vector.<Ball>();					
					}
					_balls[i] = null;
					ball.startPosition = StartPositionEnum.FREE;
					_freeBalls.push(ball);
					recteateBallAtStart(i);
				}
				
				 ball.update();
			}
			
			for each(ball in _freeBalls)
			{
				ball.update();
			}

			_world.Step(1/30, 8, 8);
			_world.ClearForces();
//			_world.DrawDebugData();
			
			if(_lastTime == -1)
			{
				_lastTime = getTimer();
			}
			var time:int = getTimer();
			_timer = _timer + (time - _lastTime);
			_lastTime = time;
			_timerTF.text = ""+int((Config.MAX_TIMER - _timer) * 0.001);
			if(_timer >= Config.MAX_TIMER)
			{
				resetGame();
			}
		}
		
		private function convertToBox(value:Number) : Number
		{
			return value / Config.BOXSCALE;
		}
		
		
		private function flagCheck(e:MouseEvent):void {
			switch (e.type){
				case "mouseDown":
					_isMouseDown = true; 
					break;
				case "mouseUp":
					_isMouseDown = false; 
					break;
			}
		}
		/* mouseDrag() - включает MouseDrag тел
		*/
		private function mouseDrag():void {
			UpdateMouseWorld(); //обновляет координаты мышки
			MouseDrag(); //реализует mouseDrag()
		}
		
		private function UpdateMouseWorld():void{
			_mouseXWorldPhys = convertToBox(stage.mouseX);  //преобразуем координаты мыши в пикселях в метры
			_mouseYWorldPhys = convertToBox(stage.mouseY); 
			
			_mouseXWorld = stage.mouseX; //записываем координаты мыши в пикселях
			_mouseYWorld = stage.mouseY; 
		}
		
		private function MouseDrag():void{
			if (_isMouseDown && !_mouseJoint){ //если кнопка мыши нажата и соединения MouseJoint не существует 
				
				var body:b2Body = GetBodyAtMouse(); //получаем ссылку на тело которое находится под курсором мыши
				
				var isDynamicBall:Boolean = true;
				for (var i:int = 0; i < _balls.length; i++)
				{
					if (_balls[i].body == body)
					{
						isDynamicBall = false;
						break;
					}
				}
				
				if (body){//если есть тело под курсором мыши 
					var md:b2MouseJointDef = new b2MouseJointDef(); //создаем настройки соединения
					md.bodyA = _world.GetGroundBody(); //один конец крепим к миру
					md.bodyB = body; //другой к телу
					md.target.Set(_mouseXWorldPhys, _mouseYWorldPhys); //соединение создается от курсора мыши
					md.collideConnected = true; //тела сталкиваются
					md.maxForce = Config.MOUSE_SPEED; //макс. сила которая может быть приложена к телу
					//md.dampingRatio = 1;
					_mouseJoint = _world.CreateJoint(md) as b2MouseJoint; //создаем соединение
					body.SetAwake(true); //будим тело
				}
			}
			
			if (!_isMouseDown){ //если кнопка мыши отпущена
				if (_mouseJoint){ //а соединение еще существует
					_world.DestroyJoint(_mouseJoint);//удаляем его
					_mouseJoint = null;
				}
			}
			
			if (_mouseJoint){ //если кнопка мыши нажата и соединение уже существует
				var p2:b2Vec2 = new b2Vec2(_mouseXWorldPhys, _mouseYWorldPhys);
				_mouseJoint.SetTarget(p2); //перемещаем соединение за курсором мыши
			}
		}
		
		private function GetBodyAtMouse(includeStatic:Boolean = false):b2Body {//определяет тело находящееся под курсором мыши
			_mousePVec.Set(_mouseXWorldPhys, _mouseYWorldPhys);//записываем текущие координаты курсора
			var aabb:b2AABB = new b2AABB();//создаем прямоугольную область
			aabb.lowerBound.Set(_mouseXWorldPhys - 0.001, _mouseYWorldPhys - 0.001); //вокруг курсора мыши
			aabb.upperBound.Set(_mouseXWorldPhys + 0.001, _mouseYWorldPhys + 0.001);
			var body:b2Body = null;
			var fixture:b2Fixture;
			
			function GetBodyCallback(fixture:b2Fixture):Boolean{
				var shape:b2Shape = fixture.GetShape(); //получаем шейп который находится под курсором
				if (fixture.GetBody().GetType() != b2Body.b2_staticBody || includeStatic){ //если тело не статическое
					var inside:Boolean = shape.TestPoint(fixture.GetBody().GetTransform(), _mousePVec); //проверяем находится ли точка-позиция курсора в рамках тела
					if (inside){ //если да
						body = fixture.GetBody(); //получаем ссылку на тело
						return false;
					}
				}
				return true;
			}
			_world.QueryAABB(GetBodyCallback, aabb); //проверяем на попадание любых тел в область aabb
			return body; //возвращаем тело
		}

	}
}