package dataManager
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class DataManager
	{
		private static const TRIM_PATTERN:RegExp = /^\s+|\s+$/g;
		public static var LEVEL:String	= "level";
		
		private var _dataXML:XML;
		private var _levelData:Vector.<Level>;

		public function DataManager()
		{
			initDB();
			parseDB();
		}
		
		private function initDB ():void
		{
			var file:File = File.applicationDirectory.resolvePath("data.xml"); 
			var fileStream:FileStream = new FileStream(); 
			fileStream.open(file, FileMode.READ); 
			_dataXML = XML(fileStream.readUTFBytes(fileStream.bytesAvailable)); 
		}
		
		private function parseDB():void
		{
			_levelData = new Vector.<Level>;
			var levels:XMLList = _dataXML.difficulty.level;
			for each (var el:XML in levels)
				_levelData.push(new Level(el));
			
		}
		
		public function get level (id:int):void
		{
			return _levelData[id];
		}
	}
}