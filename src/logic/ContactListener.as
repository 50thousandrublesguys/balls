package logic
{
	import Box2D.Collision.b2Manifold;
	import Box2D.Dynamics.b2ContactImpulse;
	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.Contacts.b2Contact;
	
	import model.Ball;
	
	public class ContactListener extends b2ContactListener
	{
		
		private var _beginCallback:Function;
		private var _endCallback:Function;
		public function set beginCallback(value:Function) : void
		{
			_beginCallback = value;
		}
		
		public function set endCallback(value:Function):void 
		{
			_endCallback = value;
		}
		
		public function ContactListener()
		{
			super();
		}
		
		override public function BeginContact(contact:b2Contact):void
		{
			_beginCallback(contact);
		}
		
		override public function EndContact(contact:b2Contact) : void
		{
			_endCallback(contact);
		}
		
		override public function PreSolve(contact:b2Contact, oldManifold:b2Manifold):void
		{
//			_beginCallback(contact);
		}
	}
}