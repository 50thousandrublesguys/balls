package view 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import model.Config;
	/**
	 * ...
	 * @author Eugene Kolesnikov aka Mitd
	 */
	public class Console extends EventDispatcher
	{
		private var _view:Sprite;
		
		private var _sensorDistanceTF:TextField;
		private var _ringSpeedTF:TextField;
		private var _ballToFingerTF:TextField;
		private var _gameTime:TextField;
		private var _ballDamping:TextField;
		private var _closeButton:Sprite;
		
		public function Console(view:Sprite) 
		{
			_view = view;
			_sensorDistanceTF = _view["tf0"] as TextField;
			_sensorDistanceTF.text = ""+Config.MAX_DISTANCE;
			_sensorDistanceTF.addEventListener(Event.CHANGE, handleTextChange);
			
			_ringSpeedTF = _view["tf1"] as TextField;
			_ringSpeedTF.text = ""+Config.RING_SPEED;
			_ringSpeedTF.addEventListener(Event.CHANGE, handleTextChange);
			
			_ballToFingerTF = _view["tf2"] as TextField;
			_ballToFingerTF.text = ""+Config.MOUSE_SPEED;
			_ballToFingerTF.addEventListener(Event.CHANGE, handleTextChange);
			
			_gameTime = _view["tf3"] as TextField;
			_gameTime.text = ""+Config.MAX_TIMER;
			_gameTime.addEventListener(Event.CHANGE, handleTextChange);
			
			_ballDamping = _view["tf4"] as TextField;
			_ballDamping.text = ""+Config.BALL_DAMPING;
			_ballDamping.addEventListener(Event.CHANGE, handleTextChange);
			
			_closeButton = _view["closeButton"] as Sprite;
			_closeButton.addEventListener(MouseEvent.CLICK, handleCloseClick);
		}
		
		private function handleCloseClick(e:MouseEvent): void
		{
			visible = false;
		}
		
		private function handleTextChange(e:Event):void 
		{
			var text:TextField = e.target as TextField;
			var value:Number = Number(text.text);
			switch(text)
			{
				case _sensorDistanceTF:
					trace("Config.MAX_DISTANCE", value);
					Config.MAX_DISTANCE = isNaN(value) ? Config.MAX_DISTANCE : value;
					break;
				case _ringSpeedTF:
					trace("Config.RING_SPEED", value);
					Config.RING_SPEED = isNaN(value) ? Config.RING_SPEED : value;
					break;
				case _ballToFingerTF:
					trace("Config.MOUSE_SPEED", value);
					Config.MOUSE_SPEED = isNaN(value) ? Config.MOUSE_SPEED : value;
					break;
				case _gameTime:
					trace("Config.MAX_TIMER", value);
					Config.MAX_TIMER = isNaN(value) ? Config.MAX_TIMER : value;
					break;
				case _ballDamping:
					trace("Config.BALL_DAMPING", value);
					Config.BALL_DAMPING = isNaN(value) ? Config.BALL_DAMPING : value;
					break;
			}
		}
		
		public function set visible(value:Boolean) : void
		{
			_view.visible = value;
		}
		
		public function get visible() : Boolean
		{
			return _view.visible;
		}
	}

}